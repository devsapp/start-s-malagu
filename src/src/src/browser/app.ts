import { createApp } from 'vue';
import { App } from '@malagu/vue';
import Root from './Root.vue';

@App(createApp(Root))
export default class {}
