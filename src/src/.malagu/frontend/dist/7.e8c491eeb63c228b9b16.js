(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[7],{

/***/ "./node_modules/@malagu/web/lib/common/module.js":
/*!*******************************************************!*\
  !*** ./node_modules/@malagu/web/lib/common/module.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @malagu/core */ "./node_modules/@malagu/core/lib/common/index.js");
var client_1 = __webpack_require__(/*! ./client */ "./node_modules/@malagu/web/lib/common/client/index.js");
__webpack_require__(/*! ./resolver */ "./node_modules/@malagu/web/lib/common/resolver/index.js");
exports.default = core_1.autoBind(function (bind) {
    bind(client_1.RestOperations).toDynamicValue(function (ctx) {
        var factory = ctx.container.get(client_1.RestOperationsFactory);
        return factory.create();
    }).inSingletonScope();
});


/***/ })

}]);
//# sourceMappingURL=7.e8c491eeb63c228b9b16.js.map