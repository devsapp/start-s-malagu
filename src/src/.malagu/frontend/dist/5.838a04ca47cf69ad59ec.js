(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ "./node_modules/@malagu/rpc/lib/common/module.js":
/*!*******************************************************!*\
  !*** ./node_modules/@malagu/rpc/lib/common/module.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @malagu/core */ "./node_modules/@malagu/core/lib/common/index.js");
var proxy_1 = __webpack_require__(/*! ./proxy */ "./node_modules/@malagu/rpc/lib/common/proxy/index.js");
var annotation_1 = __webpack_require__(/*! ./annotation */ "./node_modules/@malagu/rpc/lib/common/annotation/index.js");
var utils_1 = __webpack_require__(/*! ./utils */ "./node_modules/@malagu/rpc/lib/common/utils/index.js");
__exportStar(__webpack_require__(/*! . */ "./node_modules/@malagu/rpc/lib/common/index.js"), exports);
exports.default = core_1.autoBind(function (bind) {
    bind(annotation_1.RPC).toDynamicValue(function (ctx) {
        var _a;
        var id = (_a = ctx.currentRequest.target.getCustomTags()) === null || _a === void 0 ? void 0 : _a.find(function (m) { return m.key === annotation_1.ID_KEY; }).value;
        var path = utils_1.RpcUtil.toPath(id);
        var proxyProvider = ctx.container.get(proxy_1.ProxyProvider);
        var errorConverters = utils_1.ConverterUtil.getGlobalErrorConverters(ctx.container);
        var errorConverter = utils_1.ConverterUtil.getErrorConverters(id, ctx.container);
        if (errorConverter) {
            errorConverters.push(errorConverter);
        }
        return proxyProvider.provide(path, errorConverters);
    });
});


/***/ }),

/***/ 2:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 3:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

}]);
//# sourceMappingURL=5.838a04ca47cf69ad59ec.js.map