(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[3],{

/***/ "./node_modules/ts-loader/index.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/vue-loader/dist/index.js?!./src/browser/Root.vue?vue&type=script&lang=ts":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/ts-loader??ref--1-0!./node_modules/cache-loader/dist/cjs.js!./node_modules/vue-loader/dist??ref--12-1!./src/browser/Root.vue?vue&type=script&lang=ts ***!
  \***************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const vue_1 = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.runtime.esm-bundler.js");
const rpc_1 = __webpack_require__(/*! @malagu/rpc */ "./node_modules/@malagu/rpc/lib/common/index.js");
const welcome_protocol_1 = __webpack_require__(/*! ../common/welcome-protocol */ "./src/common/welcome-protocol.ts");
const Root = vue_1.defineComponent({
    data() {
        return {
            message: 'loading...'
        };
    },
    mounted() {
        this.load();
    },
    methods: {
        async load() {
            const welcomeServer = rpc_1.RpcUtil.get(welcome_protocol_1.WelcomeServer);
            this.message = await welcomeServer.say();
        }
    }
});
exports.default = Root;


/***/ }),

/***/ "./node_modules/vue-loader/dist/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/vue-loader/dist/index.js?!./src/browser/Root.vue?vue&type=template&id=65de9762":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/dist/templateLoader.js??ref--5!./node_modules/cache-loader/dist/cjs.js!./node_modules/vue-loader/dist??ref--12-1!./src/browser/Root.vue?vue&type=template&id=65de9762 ***!
  \*******************************************************************************************************************************************************************************************************/
/*! exports provided: render */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.runtime.esm-bundler.js");


function render(_ctx, _cache, $props, $setup, $data, $options) {
  return (Object(vue__WEBPACK_IMPORTED_MODULE_0__["openBlock"])(), Object(vue__WEBPACK_IMPORTED_MODULE_0__["createBlock"])("div", null, Object(vue__WEBPACK_IMPORTED_MODULE_0__["toDisplayString"])(_ctx.message), 1 /* TEXT */))
}

/***/ }),

/***/ "./src/browser/Root.vue":
/*!******************************!*\
  !*** ./src/browser/Root.vue ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Root_vue_vue_type_template_id_65de9762__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Root.vue?vue&type=template&id=65de9762 */ "./src/browser/Root.vue?vue&type=template&id=65de9762");
/* harmony import */ var _Root_vue_vue_type_script_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Root.vue?vue&type=script&lang=ts */ "./src/browser/Root.vue?vue&type=script&lang=ts");
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _Root_vue_vue_type_script_lang_ts__WEBPACK_IMPORTED_MODULE_1__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _Root_vue_vue_type_script_lang_ts__WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));



_Root_vue_vue_type_script_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"].render = _Root_vue_vue_type_template_id_65de9762__WEBPACK_IMPORTED_MODULE_0__["render"]
/* hot reload */
if (false) {}

_Root_vue_vue_type_script_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"].__file = "src/browser/Root.vue"

/* harmony default export */ __webpack_exports__["default"] = (_Root_vue_vue_type_script_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"]);

/***/ }),

/***/ "./src/browser/Root.vue?vue&type=script&lang=ts":
/*!******************************************************!*\
  !*** ./src/browser/Root.vue?vue&type=script&lang=ts ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_ts_loader_index_js_ref_1_0_node_modules_cache_loader_dist_cjs_js_node_modules_vue_loader_dist_index_js_ref_12_1_Root_vue_vue_type_script_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/ts-loader??ref--1-0!../../node_modules/cache-loader/dist/cjs.js!../../node_modules/vue-loader/dist??ref--12-1!./Root.vue?vue&type=script&lang=ts */ "./node_modules/ts-loader/index.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/vue-loader/dist/index.js?!./src/browser/Root.vue?vue&type=script&lang=ts");
/* harmony import */ var _node_modules_ts_loader_index_js_ref_1_0_node_modules_cache_loader_dist_cjs_js_node_modules_vue_loader_dist_index_js_ref_12_1_Root_vue_vue_type_script_lang_ts__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_ts_loader_index_js_ref_1_0_node_modules_cache_loader_dist_cjs_js_node_modules_vue_loader_dist_index_js_ref_12_1_Root_vue_vue_type_script_lang_ts__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (default from non-harmony) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _node_modules_ts_loader_index_js_ref_1_0_node_modules_cache_loader_dist_cjs_js_node_modules_vue_loader_dist_index_js_ref_12_1_Root_vue_vue_type_script_lang_ts__WEBPACK_IMPORTED_MODULE_0___default.a; });
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_ts_loader_index_js_ref_1_0_node_modules_cache_loader_dist_cjs_js_node_modules_vue_loader_dist_index_js_ref_12_1_Root_vue_vue_type_script_lang_ts__WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_ts_loader_index_js_ref_1_0_node_modules_cache_loader_dist_cjs_js_node_modules_vue_loader_dist_index_js_ref_12_1_Root_vue_vue_type_script_lang_ts__WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 

/***/ }),

/***/ "./src/browser/Root.vue?vue&type=template&id=65de9762":
/*!************************************************************!*\
  !*** ./src/browser/Root.vue?vue&type=template&id=65de9762 ***!
  \************************************************************/
/*! exports provided: render */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_dist_templateLoader_js_ref_5_node_modules_cache_loader_dist_cjs_js_node_modules_vue_loader_dist_index_js_ref_12_1_Root_vue_vue_type_template_id_65de9762__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/vue-loader/dist/templateLoader.js??ref--5!../../node_modules/cache-loader/dist/cjs.js!../../node_modules/vue-loader/dist??ref--12-1!./Root.vue?vue&type=template&id=65de9762 */ "./node_modules/vue-loader/dist/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/vue-loader/dist/index.js?!./src/browser/Root.vue?vue&type=template&id=65de9762");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_dist_templateLoader_js_ref_5_node_modules_cache_loader_dist_cjs_js_node_modules_vue_loader_dist_index_js_ref_12_1_Root_vue_vue_type_template_id_65de9762__WEBPACK_IMPORTED_MODULE_0__["render"]; });



/***/ }),

/***/ "./src/browser/app.ts":
/*!****************************!*\
  !*** ./src/browser/app.ts ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const vue_1 = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.runtime.esm-bundler.js");
const vue_2 = __webpack_require__(/*! @malagu/vue */ "./node_modules/@malagu/vue/lib/browser/index.js");
const Root_vue_1 = __webpack_require__(/*! ./Root.vue */ "./src/browser/Root.vue");
let default_1 = class {
};
default_1 = __decorate([
    vue_2.App(vue_1.createApp(Root_vue_1.default))
], default_1);
exports.default = default_1;


/***/ }),

/***/ "./src/browser/module.ts":
/*!*******************************!*\
  !*** ./src/browser/module.ts ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(/*! ./app */ "./src/browser/app.ts");
const core_1 = __webpack_require__(/*! @malagu/core */ "./node_modules/@malagu/core/lib/common/index.js");
exports.default = core_1.autoBind();


/***/ }),

/***/ "./src/common/welcome-protocol.ts":
/*!****************************************!*\
  !*** ./src/common/welcome-protocol.ts ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.WelcomeServer = void 0;
exports.WelcomeServer = Symbol('WelcomeServer');


/***/ }),

/***/ 2:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 3:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

}]);
//# sourceMappingURL=3.dfacfb19fbd7abc6db49.js.map