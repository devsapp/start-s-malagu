## 介绍
本项目是 [s-malagu](https://github.com/devsapp/s-malagu) 组件的启动项目，你可以通过对s.yaml进行配置进行对malagu框架的部署


malagu是一个现代的前端开发框架，具备灵活扩展，serverless 友好的特性[malagu 相关介绍](https://www.yuque.com/cellbang/malagu) 

## 使用说明
初始化
```
s init devsapp/start-s-malagu
```
或者查看文档

```
cd <project dir> && s malagu -d
```
执行测试
```
cd <project dir> && s dev
```
